const http = require("http")
const port = 8001

function requestHandler (req, res) {
    if (req.url == "/") {
        res.writeHead(200, {"Content-Type": "text/html"})
        res.end("Welcome to the homepage!")
    }
    // about page
    else if (req.url == "/contact") {
        res.writeHead(200, {"Content-Type": "text/html"})
        res.end("Welcome to the contact page1")
    } 
    // 404 not found
    else {
        res.writeHead(404, {"Content-Type": "text/plain"})
        res.end("404 error! File not found.")
    }
}

const server = http.createServer(requestHandler)
server.listen(port, function(err) {
    if (err) {
        console.log(err)
        return
    }

    console.log("Server is up and running: ", port)
})

console.log("Server has started")