Link tutorial [how to write a simple cli in nodejs](https://dev.to/kennymark/how-to-write-a-simple-cli-in-node-js-4hnh)

Objective :
- Ask what type unit do you want to create. Could be one of atom, molecule, organism etc
- Ask for name of the file and format it
- Get answers above and create a folder and file based on these answers 
- Use created templates and replace 

How to run:
- `npm install`
- `node sample-cli.js`